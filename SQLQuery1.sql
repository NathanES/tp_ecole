
--Question 1
SELECT NOM, PRENOM,convert(varchar,DATE_NAISSANCE,111) datedenaissance
FROM ELEVES


--Question 2
SELECT *
FROM ACTIVITES


--Question 3
SELECT SPECIALITE
From PROFESSEURS


--Question 4
SELECT NOM, PRENOM
FROM ELEVES
WHERE (POIDS < 45 and ANNEE = 1) OR ANNEE = 2


--Question 5
SELECT NOM
FROM ELEVES
WHERE POIDS between 60 and 80


--Question 6
SELECT NOM
FROM PROFESSEURS
WHERE SPECIALITE ='Po�sie' or SPECIALITE='sql'


--Question 7
SELECT nom
From ELEVES
where NOM like 'L%'


--Question 8
SELECT NOM
from PROFESSEURS
WHERE SPECIALITE IS NULL


--Question 9
SELECT NOM,
CASE
WHEN SPECIALITE IS NULL THEN 'Sp�cialit� inconnue'
when SPECIALITE IS NOT NUll then SPECIALITE
END
FROM PROFESSEURS
--est �gale � :
SELECT NOM,
Isnull(Specialite,'specialit� inconnue')
FROM PROFESSEURS


--Question 10
SELECT eleves.NOM, eleves.PRENOM
FROM ELEVES
INNer JOIN ACTIVITES_PRATIQUEES ON ELEVES.NUM_ELEVE = ACTIVITES_PRATIQUEES.NUM_ELEVE
Where ACTIVITES_PRATIQUEES.NIVEAU = 1 and ACTIVITES_PRATIQUEES.NOM='surf'

--Egale �

select NOM,Prenom
from ELEVES
Where NUM_ELEVE in (
		select NUM_ELEVE
		From ACTIVITES_PRATIQUEES
		Where NIVEAU = 1 and NOM = 'surf')


--Question 11
SELECT elv.nom
from eleves elv
Inner join ACTIVITES_PRATIQUEES AP ON elv.NUM_ELEVE = AP.NUM_ELEVE
INNER JOIN ACTIVITES A ON AP.NIVEAU = A.NIVEAU
and ap.NOM = a.NOM
WHERE A.EQUIPE = 'Equipe 1'


--Question 12
SELECT NOM, ISNULL(specialite,'specialit� inconnue')
FROM PROFESSEURS
WHERE ISNull(specialite,'specialit� inconnue') in (
		SELECT IsNull(SPECIALITE,'specialit� inconnue')
		FROM PROFESSEURS
		group by SPECIALITE
		having count(ISnull(Specialite,'sp�cialit� inconnue')) = 2)

--Question 13
SELECT NOM,
SALAIRE_ACTUEL/12 salaireMensuel,
(SALAIRE_ACTUEL-SALAIRE_BASE)/DATEDIFF(month, DATE_ENTREE,DER_PROM) promotionMensuel
FROM PROFESSEURS

--Question 14
Select prof.NOM
From professeurs prof 
where (SALAIRE_ACTUEL*100/SALAIRE_BASE) > 125 

--Question 15
Select (res.POINTS*100/20) PointSur100
from RESULTATS res
Inner join ELEVES elv
on res.NUM_ELEVE = elv.NUM_ELEVE
Where elv.NOM = 'Burton'

--Question 16
Select Sum(poids)/count(NUM_ELEVE) PoidsMoyen1ereAnnee
FROM ELEVES
Group by ANNEE
Having ANNEE = 1

--Question 17

Select sum(res.POINTS) sommeNotes
From RESULTATS res
Where res.NUM_ELEVE = 3

--Question 18
Select Min(POINTS) Minimum, MAX(Points) Maximum
From RESULTATS
Inner join Eleves
On ELEVES.NUM_ELEVE = RESULTATS.NUM_ELEVE
Where ELEVES.nom = 'Irving'

--Question 19
Select count(NUM_ELEVE) 'nombre d"eleve'
From ELEVES
Where ANNEE = 2

--Question 20
SELEct concat(avg(salaire_actuel)*100/AVG(salaire_base)-100,'%')
From PROFESSEURS
group by SPECIALITE
having SPECIALITE ='C/C++'

--Question 21

SELECT Datepart(year,PROF.DATE_ENTREE),Datepart(year,PROF.DER_PROM), Datediff(YEAR,PROF.DATE_ENTREE,PROF.DER_PROM) TempsEcoulee
FROM PROFESSEURS PROF  

--Question 22

SElect Avg(datediff(year,DATE_NAISSANCE,GETDATE()))
from ELEVES

--Question 23

Select prof.NOM, prof.DATE_ENTREE
from PROFESSEURS prof
Where 50 < DATEDIFF(month,DER_PROM,GETDATE()) 

--Question 24

Select *
from ELEVES
Order by ANNEE ASC, NOM ASC

--Question 25

SELECT points*5 notesSur100
FROM  ELEVES elv
Inner Join RESULTATS res
On res.NUM_ELEVE = elv.NUM_ELEVE
where elv.NOM='Irving'
Order by POINTS Desc

--Question 26
-- cast permet de changer de passer de int � float par exemple
SELECT NOM,AVG(cast(points as float)) moyenneNote
FROM ELEVES elv
Inner join RESULTATS res
On elv.NUM_ELEVE = res.NUM_ELEVE
group by elv.NOM

--Question 27
SELECT NOM, POIDS
FROM ELEVES
where annee = 1 and POIDS > (
		Select max(poids)
		From ELEVES
		where annee = 2
	--	group by ANNEE
	--	having ANNEE = 2
	)

--Question 28

SELECT MIN(res.POINTS) Minimum ,MAX(res.Points) Maximum ,AVG(res.points) Moyenne, COURS.NOM
FROM RESULTATS res
Inner join COURS
On COURS.NUM_COURS = res.NUM_COURS
group by res.NUM_COURS,cours.NOM

--Question 29

SELECT    C.NOM,
        E.NOM, 
        MIN(R.POINTS) NOTE_MIN, 
        MAX(R.POINTS) NOTE_MAX,
        AVG(CAST(R.POINTS AS FLOAT)) NOTE_MOY
FROM ELEVES E
INNER JOIN RESULTATS R 
ON R.NUM_ELEVE=E.NUM_ELEVE
INNER JOIN COURS C 
ON C.NUM_COURS=R.NUM_COURS
GROUP BY R.NUM_COURS, E.NOM, C.NOM
ORDER BY C.NOM, E.NOM ASC




--Question 30

Select prof.NOM
from PROFESSEURS prof
Inner join CHARGE cha
on prof.NUM_PROF = cha.NUM_PROF
where not exists (select *
				from CHARGE
				where    CHARGE.NUM_PROF = prof.NUM_PROF
				and NUM_COURS = 1)

/*
--Exemple cour
SELECT COURS.NOM, res.POINTS,elv.NOM,elv.PRENOM
FROM ELEVES elv
INNER JOIN RESULTATS res
ON res.NUM_ELEVE = elv.NUM_ELEVE
Inner join COURS
ON COURS.NUM_COURS = res.NUM_COURS
*/